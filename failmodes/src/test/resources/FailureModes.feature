@Failuremode
Feature: Failure Modes
  Scenario: WeGood Temporarily Unavailable
    Given the Hello and WeGood services are deployed in cluster "moody-cluster" and available
    And WeGood becomes unavailable for 2 seconds
    
    When I make a request to Hello
    
    Then I receive a runtime exception response from "hello" indicating a server problem
